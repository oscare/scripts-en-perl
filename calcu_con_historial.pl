#!/usr/bin/perl

use Switch;

$in=<STDIN>;
@history = ();
while ($in ne "salir\n"){
	if ($in eq "h\n"){
		print "Historial:\n";
		foreach $h (@history) {
			print "\t$h\n";
		}
	}
	else{
		@valores = split(' ', $in);
		$res = 0;
		switch($valores[1]) {
			case '+' {
				$res = $valores[0] + $valores[2];
				print "$res \n";
			}
			case '-' {
				$res = $valores[0] - $valores[2];
				print "$res \n";
			}
			case '*' {
				$res = $valores[0] * $valores[2];
				print "$res \n";
			}
			case '/' {
				$res = $valores[0] / $valores[2];
				print "$res \n";
			}
			else{
				print "Operacion no valida\n";
			}
		}
	}
	
	push(@history, $valores[0]." ".$valores[1]. " ".$valores[2]." = ".$res);
	$in = <STDIN>;
}

	
