#!/usr/bin/perl

#---------------
#   UNAM-CERT
#   PBSI
#   Oscar Espinosa
#---------------

use LWP::Simple;

#Archivo de sitios activos
my $active_sites = 'active_sites.txt';
my $phishing_sites = 'pishing.txt';
die ("No se pudo abrir el archivo\n") unless open(ACTIVES, "$active_sites");
die ("No se puede abrir $phishing_sites") unless(open(PSH, ">$phishing_sites"));
#Arreglo de palabras a buscar
@pishing = qw(requires password verification contrasena change required immediately security alert);

#Copiamos los renglones y quitamos el salto de linea
chomp(@sites = <ACTIVES>);
close(ACTIVES);
@chosen_sites=();
$size= scalar @sites;
#elegimos 5 sitios al azar
for (my $i=0;$i<5;$i++){
	push(@chosen_sites,@sites[int(rand($size-1))]);
}

$flag = False;
foreach $site (@chosen_sites) {
    $content = get($site);
    print "Sitio elegido: $site\n";
    #Por cada sitio buscamos cada elemento de @pishing
    foreach $pish (@pishing) {
        #Si se hace match, se imprime el posible pishing
        $content =~ /$pish/ ? print ("\tPosible pishing: $pish\n"):next; 
        $content =~ /$pish/ ? $flag = True: next;
    }
    if ($flag eq True){
        print PSH "$site\n";
    }
    $flag = False
}

close (PSH);