#!/usr/bin/perl

#---------------
#   UNAM-CERT
#   PBSI
#   Oscar Espinosa
#---------------

#   Script que ejecuta un analisis de red con Nmap generando
#   estadisticas de los puertos abiertos en los distintos
#   host encontrados en la red

use strict;
use warnings;

#Estos son los nombres de archivos que se usan para los
#escaneos de red y de cada host y el nombre del archivo del
#reporte
my $temp_net_scanner = 'net_scanner.tmp';
my $tmp_host_scan = 'host_scan.tmp';
my $report = 'reporte_escaneo.txt';


sub printError {
    #Funcion que imprime el mensaje de error dado
    warn "\n".$_[0];
}

#Hash global para que no se sobreescriba a cada invocacion de get_stadistics
my %open_ports = ();
sub get_stadistics {
        # Funcion que obtiene los puertos abiertos del archivo donde
        # se redirigio la salida de nmap y el valor lo almacena en un
        # hash donde se cuentan las incidencias de tal puerto
    die ("No se pudo abrir el archivo $_[0]\n") unless open(HSTSCN, "$_[0]");
    my $line = '';
    #Las primeras 5 lineas son mensajes de Nmap
    for (my $i = 0 ; $i<5; $i++){
        $line = <HSTSCN>;
    }
    #Se comienzan a leer los puertos escuchando del host respectivo
    while ($line = <HSTSCN>) {
        #Se termina el ciclo si llegamos a las ultimas dos lineas
        if ($line =~ /Nmap done/ || $line eq "\n"){
            last;
        }
        #Obtenemos solo el numero de puerto
        my @port = split(/ /, $line);
        my @num_port = split(/\//,$port[0]);

        #Si existe aumentamos su contador, si no lo iniciamos en 1
        if (exists($open_ports{$num_port[0]})) {
            $open_ports{$num_port[0]} += 1;
        }
        else {
            $open_ports{$num_port[0]} = 1;
        }
    }
    close(HSTSCN);
}

sub printReport {
        # Funcion que imprime el puerto y el numero de veces que se encontro
        # en la busqueda
        # Recibe como parametro el nombre del archivo donde se escribira el
        # reporte

    die ("No se pudo abrir el archivo $_[0]\n") unless open(REP, ">$_[0]");
    
    print "\nEscribiendo reporte en $_[0]\n";

    print REP "puerto  incidencias";

    foreach my $k (keys(%open_ports)) {
        print REP "\n$k $open_ports{$k}";
    }

    close(REP);
}

sub scan_host {
        # Funcion que hace el escaneo de puertos a cada host disponible en
        # la red
        #     El primer argumento es el archivo realizado del escaneo de red
        #     El segundo es el nombre del archivo donde se escribira el escane
        #     de puertos
    #Se obtiene en un arreglo los host activos encontrados
    my @hosts = get_up_hosts($_[0]);
    print "\n Lista de host disponibles encontrados en la red: \n";
    print "@hosts";

    foreach my $host (@hosts) {
        print "\nEscaneando puertos de $host";
        my $scan = system("nmap -T4 -F $host > $_[1]");
        get_stadistics($_[1]);
    }

    printReport($report);   

}

sub get_up_hosts {
        # Funcion que analiza el archivo dado como parametro $_[0]
        # y obtiene el host y si estan arriba.
        # Todos los host arriba los agrega a un arreglo
        # Retorna el arreglo con los host disponibles
    die ("No se pudo abrir el archivo $_[0]\n") unless open(NETSCN, "$_[0]");
    #Omitimos la primer linea del archivo
    my $line = <NETSCN>;
    my $host = '';
    my @up_hosts = ();
    while ($line = <NETSCN>) {
        #Se termina el ciclo si llegamos a la ultima linea
        if ($line =~ /Nmap done/){
            last;
        }
        #Si se encuentra la regex, se hace la sustitucion y se agrega al arreglo
        if ($line =~ /Nmap scan report for/) {
            $line =~ s/Nmap scan report for ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/$1/;
            $host = $line;
            chomp $host;
        }

        #Se lee la linea siguiente, la que nos dice si esta disponible o no
        $line = <NETSCN>;
        #Si esta disponible, se agrega al arreglo
        if ($line =~/Host is up/) {
            push(@up_hosts, $host);
        }
    }

    close(NETSCN);
    #system("rm $_[0]");
    return @up_hosts;
}

sub nmap_main {
        # Funcion que hace el escaneo de red usando NMAP
        # La salida la redirige al archivo dado como parametro en $_[0]
        # Si el comando termina sin error, se ejecuta la funcion que obtiene
        # los host disponibles de este archivo

    my $net = "$ARGV[0]";
    chomp($net);
    print "\nHaciendo el escaneo de la red con NMAP\n";
    my $scanner = system("nmap -T4 -sP $net/24 > $_[0]");
    $scanner == 0 ? get_up_hosts($_[0]):printError('No se pudo concluir el analisis de la red dada');
}

nmap_main($temp_net_scanner);
scan_host($temp_net_scanner, $tmp_host_scan);