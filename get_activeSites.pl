#!/usr/bin/perl

#---------------
#   UNAM-CERT
#   PBSI
#   Oscar Espinosa
#---------------

#Script que almacena en otro archivo los sitios activos
#de $feed_file

use LWP::Simple;
my $feed_file = 'feed2.txt';
my $active_sites = 'active_sites.txt';

die "No se puede abrir $active_sites" unless(open(ACTIVES, ">$active_sites"));
die "No se puede abrir $feed_file" unless(open(FEEDF, "$feed_file"));
#Se puedieron abrir correctamente ambos archivos
my $content = "";
foreach $url (<FEEDF>){
    #Si el sitio no esta levantado, get devuelve undefined
    $content = get($url);
    print ACTIVES "$url" if defined $content;
    print "No activo: $url" if ! defined $content;
}
close(ACTIVES);
close(FEEDF);