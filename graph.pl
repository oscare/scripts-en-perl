#!/usr/bin/perl

#---------------
#   UNAM-CERT
#   PBSI
#   Oscar Espinosa
#---------------

#   Script que grafica las estadisticas generadas en la
#   practica 1

#   codigo basado en: https://perlmaven.com/creating-bar-graphs-using-perl-gd-graph

use strict;
use warnings;
 
use GD::Graph::bars;
use GD::Graph::Data;
 
my @puertos = ();
my @incidencias = ();
my $network = '';
my $report = 'reporte_escaneo.txt';

sub do_graph {
        #Funcion que genera una imagen con la grafica de
        #los valores obtenidos en la practica 1

    #Se llena con las referencias a los arreglos
    my $data = GD::Graph::Data->new([
        \@puertos,
        \@incidencias,
    ]) or die GD::Graph::Data->error;
    
    my $graph = GD::Graph::bars->new;
    
    $graph->set( 
        x_label         => 'puertos',
        y_label         => 'incidencias',
        title           => "Puertos abiertos en la red $network",
    
        #y_max_value     => 7,
        #y_tick_number   => 8,
        #y_label_skip    => 3,
    
        #x_labels_vertical => 1,
    
        #bar_spacing     => 10,
        #shadow_depth    => 4,
        #shadowclr       => 'dred',
    
        #transparent     => 0,
    ) or die $graph->error;
    
    #Se genera la grafica con los valores modificados arriba
    $graph->plot($data) or die $graph->error;
    
    #Se crea el archivo donde se escribira la grafica
    my $file = "red_$network.png";
    open(GPF, '>', $file) or die "\nNo se puede abrir el archivo $file";
    binmode GPF;
    print GPF $graph->gd->png;
    close GPF;
}

sub read_file {
        #Funcion que lee el archivo del reporte y agrega a un
        #arreglo los puertos y a otro sus incidencias
    die ("No se pudo abrir el archivo $_[0]\n") unless open(REP, "$_[0]");

    $network = <REP>;
    chomp ($network);
    my $line = <REP>;
    while ($line = <REP>) {
        (my $port, my $inc) = split(/\s/, $line);
        push(@puertos, $port);
        push(@incidencias, $inc);
    }
    close (REP);
}

read_file ($report);
do_graph();